function preload() {
  soundFormats('mp3', 'ogg');
  mySound = loadSound('data/horn');
  soundleft = loadSound('data/waves3bb');
  soundright = loadSound('data/old_ship');
  drops=loadImage('data/eik3.jpg');
  // edw fortwnw thn maska
  imgMask = loadImage('data/bmask.png'); 



}

function setup() {
  let cnv = createCanvas(windowWidth, windowHeight);
  background(10);
    amplitudeSound = new p5.Amplitude();
    
   imgRatio=drops.width/drops.height;


}

function draw(){
  
  imageMode(CORNER);
  image(drops,0, 0,width,width/imgRatio);


  fill(255);
 

   vol = amplitudeSound.getLevel();
  let w = map(vol, 0, 1, 2, 50);
//auto xreiazetai gia na einai to kentro ths eikonas ths maskas
//dhladh h trupa pou nai sto kentro na nai sto mouseX,mouseY
  imageMode(CENTER);

  let imgMaskRatio=imgMask.width/imgMask.height;
  //edw vazw thn maska meta apo auto pou thelw na maskarei
  //epeidh ama thn kounas tha fainontai ta oria ths pollaplasiase th px 5*windowWidth 
  //gia na nai polu pio megalh apton kamva sou kai na mhn fainontai ta oria ths
  image(imgMask, mouseX,mouseY,5*windowWidth,5*windowWidth/imgRatio);
}

function keyPressed() {
  if (keyCode === ENTER) {
    
    let fs = fullscreen();
    fullscreen(!fs);
  
  
  } else  {
  soundleft.play();
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function mousePressed() {
    
     if (mySound.isPlaying()){
      mySound.pause();
    }else{
      mySound.loop();
    }
  //}
}
