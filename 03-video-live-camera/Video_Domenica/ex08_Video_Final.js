let playing = true;
let imgA, imgB;
let video1, video2;
let imgRatio1, imgRatio2;
let vidRatio1, vidRatio2;
let vTint=0;

function preload() {
  imgA = loadImage('data/eik1.jpg'); // Load the image
  imgB = loadImage('data/eik2.jpg'); // Load the image
  video1 = createVideo('data/VID2.MP4');
  video2 = createVideo('data/VID4.MP4');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  // specify multiple formats for different browsers
while (video1.height == 0) delay(2);
while (video2.height == 0) delay(2);
  video1.hide();
  video2.hide();
  imgRatio1=imgA.width/imgA.height;
  imgRatio2=imgB.width/imgB.height;
  vidRatio1=video1.width/video1.height;
  vidRatio2=video2.width/video2.height;

  //video1.size(windowWidth,windowWidth/vidRatio);
  //video2.size(windowWidth,windowWidth/vidRatio);
}
function draw() {
  //background(0);
  //image(video1,0,height/3,video1.width/2,video1.height/2);
  //image(video2,video1.width/2,height/3,video2.width/2,video2.height/2);
  //image (imgA, 0, 0, windowWidth, windowWidth/imgRatio);
  // tint(map(mouseX, 0, width, 0, 255), map(mouseY, 0, height, 0, 255));  
  // image(imgB, 0, 0, windowWidth, windowWidth/imgRatio2);
  if (vTint===1) {
    background(0, 255, 0);
    image(video1, 0, 0, windowWidth, windowWidth/vidRatio1);
    //tint(map(mouseX, 0, width, 0, 255), map(mouseY, 0, height, 0, 255));
    //image (imgA, 0, 0, windowWidth, windowWidth/imgRatio1);
  } else
    if (vTint===2) { 
      background(255, 0, 0);
      image(video2, 0, 0, windowWidth, windowWidth/vidRatio2);
      //tint(map(mouseX, 0, width, 0, 255), map(mouseY, 0, height, 0, 255));
      //image (imgB, 0, 0, windowWidth, windowWidth/imgRatio2);
    }
}



function keyPressed() {
  if (playing) {
    vTint=1;
    video2.stop();
    video1.play();
    video1.loop();
    while (video1.height == 0) delay(2);
    video1.volume(1);
  } else {
    vTint=2;
    video1.stop();
    video2.play();
    video2.loop();
    while (video2.height == 0) delay(2);
    video2.volume(1);
  }
  playing = !playing;
}
